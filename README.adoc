== Script that mirrors gpt4free to codeberg servers

- https://github.com/xtekky/gpt4free[gpt4free]
- https://codeberg.org/gpt4free-mirror/gpt4free[gpt4free-mirror]
- https://chat.chatbot.sex[web application]
- https://framagit.org/fodenf/mirror-gpt4free[mirror-script]

== License

gpt4free itself is publish under a https://codeberg.org/gpt4free-mirror/gpt4free/src/branch/main/LICENSE[GPL-3],
all script and developments are distributed under link:LICENSE[0BSD]
