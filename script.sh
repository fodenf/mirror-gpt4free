#!/bin/env sh
set -euxo pipefail

[[ $1 =~ [^/]*$ ]] && name=${BASH_REMATCH[0]}

if [[ -d "$name.git" ]]; then
  cd "$name.git"
  git fetch --prune --tags --prune-tags "$1" "+refs/*:refs/*"
else
  git clone --mirror $1 $name.git
  cd "$name.git"
fi

git push --all $2
git push --tags $2
